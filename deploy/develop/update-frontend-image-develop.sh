docker pull $(docker service inspect sisu-develop_frontend --format='{{index .Spec.Labels "com.docker.stack.image"}}')
docker service update --force sisu-develop_frontend --image $(docker service inspect sisu-develop_frontend --format='{{index .Spec.Labels "com.docker.stack.image"}}')
