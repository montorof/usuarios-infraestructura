--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.16
-- Dumped by pg_dump version 11.3

-- Started on 2020-04-14 09:53:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 17475)
-- Name: autenticacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA autenticacion;


ALTER SCHEMA autenticacion OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 192 (class 1259 OID 17524)
-- Name: acceso; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.acceso (
    id_usuario integer NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE autenticacion.acceso OWNER TO postgres;

--
-- TOC entry 2219 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE acceso; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.acceso IS 'Tabla de accesos adicionales al rol por defectopara el usuario';


--
-- TOC entry 2220 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN acceso.id_usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.acceso.id_usuario IS 'id del usuario';


--
-- TOC entry 2221 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN acceso.id_rol; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.acceso.id_rol IS 'id del rol';


--
-- TOC entry 196 (class 1259 OID 28837)
-- Name: arcgis; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.arcgis (
    id_arcgis integer NOT NULL,
    id_sistema integer NOT NULL,
    usuario character varying(32) NOT NULL,
    clave character varying(256) NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    nombre character varying(32) NOT NULL
);


ALTER TABLE autenticacion.arcgis OWNER TO postgres;

--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.id_arcgis; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.id_arcgis IS 'Representa un valor consecutivo y unico que identifica el registro';


--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.id_sistema IS 'id del sistema';


--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.usuario IS 'usuario del srevidor  arcgis';


--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.clave; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.clave IS 'clave del servidor arcgis';


--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.activo; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.activo IS 'Indica si el sistema arcgis está activo o no';


--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN arcgis.nombre; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.arcgis.nombre IS 'Nombre del sistema arcgis';


--
-- TOC entry 195 (class 1259 OID 28835)
-- Name: arcgis_id_arcgis_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.arcgis_id_arcgis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.arcgis_id_arcgis_seq OWNER TO postgres;

--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 195
-- Name: arcgis_id_arcgis_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.arcgis_id_arcgis_seq OWNED BY autenticacion.arcgis.id_arcgis;


--
-- TOC entry 187 (class 1259 OID 17497)
-- Name: permiso; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.permiso (
    id_permiso integer NOT NULL,
    id_sistema integer NOT NULL,
    descripcion character varying(128) NOT NULL,
    nombre_corto character varying(32) NOT NULL
);


ALTER TABLE autenticacion.permiso OWNER TO postgres;

--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 187
-- Name: TABLE permiso; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.permiso IS 'Contiene informacion sobre los permisos de cada rol';


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN permiso.id_permiso; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.permiso.id_permiso IS 'Representa un valor consecutivo y unico que identifica el registro';


--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN permiso.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.permiso.id_sistema IS 'id del sistema';


--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN permiso.descripcion; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.permiso.descripcion IS 'Descripcion del permiso';


--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN permiso.nombre_corto; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.permiso.nombre_corto IS 'Nombre corto del permiso para identificar en la aplicaci��n';


--
-- TOC entry 186 (class 1259 OID 17495)
-- Name: permiso_id_permiso_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.permiso_id_permiso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.permiso_id_permiso_seq OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 186
-- Name: permiso_id_permiso_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.permiso_id_permiso_seq OWNED BY autenticacion.permiso.id_permiso;


--
-- TOC entry 183 (class 1259 OID 17478)
-- Name: pregunta_desafio; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.pregunta_desafio (
    id_pregunta integer NOT NULL,
    descripcion character varying(128) NOT NULL
);


ALTER TABLE autenticacion.pregunta_desafio OWNER TO postgres;

--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE pregunta_desafio; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.pregunta_desafio IS 'Tabla que guarda las preguntas de desafio';


--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN pregunta_desafio.id_pregunta; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.pregunta_desafio.id_pregunta IS 'id pregunta de desafio';


--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN pregunta_desafio.descripcion; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.pregunta_desafio.descripcion IS 'descripción de la pregunta de desafio';


--
-- TOC entry 182 (class 1259 OID 17476)
-- Name: pregunta_desafio_id_pregunta_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.pregunta_desafio_id_pregunta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.pregunta_desafio_id_pregunta_seq OWNER TO postgres;

--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 182
-- Name: pregunta_desafio_id_pregunta_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.pregunta_desafio_id_pregunta_seq OWNED BY autenticacion.pregunta_desafio.id_pregunta;


--
-- TOC entry 189 (class 1259 OID 17506)
-- Name: rol; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.rol (
    id_rol integer NOT NULL,
    id_sistema integer NOT NULL,
    descripcion character varying(128) DEFAULT 'descripcion-rol'::character varying NOT NULL,
    nombre character varying(64) DEFAULT 'nombre-rol'::character varying NOT NULL
);


ALTER TABLE autenticacion.rol OWNER TO postgres;

--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 189
-- Name: TABLE rol; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.rol IS 'Contiene informacion sobre los roles del sistema';


--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN rol.id_rol; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol.id_rol IS 'Representa un valor consecutivo y Unico que identifica el registro';


--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN rol.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol.id_sistema IS 'id del sistema';


--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN rol.descripcion; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol.descripcion IS 'descripcion del rol';


--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN rol.nombre; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol.nombre IS 'Nombre del rol';


--
-- TOC entry 188 (class 1259 OID 17504)
-- Name: rol_id_rol_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.rol_id_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.rol_id_rol_seq OWNER TO postgres;

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 188
-- Name: rol_id_rol_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.rol_id_rol_seq OWNED BY autenticacion.rol.id_rol;


--
-- TOC entry 194 (class 1259 OID 17534)
-- Name: rol_permiso; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.rol_permiso (
    id_rol integer NOT NULL,
    id_permiso integer NOT NULL
);


ALTER TABLE autenticacion.rol_permiso OWNER TO postgres;

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE rol_permiso; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.rol_permiso IS 'Tabala de relacion entre Rol y Permiso';


--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN rol_permiso.id_rol; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol_permiso.id_rol IS 'id del rol';


--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN rol_permiso.id_permiso; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.rol_permiso.id_permiso IS 'id del permiso';


--
-- TOC entry 185 (class 1259 OID 17486)
-- Name: sistema; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.sistema (
    id_sistema integer NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion character varying(256) NOT NULL,
    cant_preg_desafio integer DEFAULT 0 NOT NULL,
    dias_cambio_clave integer DEFAULT 0 NOT NULL,
    activo boolean DEFAULT true NOT NULL
);


ALTER TABLE autenticacion.sistema OWNER TO postgres;

--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.sistema IS 'Tabla que guarda la información de los sistemas';


--
-- TOC entry 2249 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.id_sistema IS 'id del sistema';


--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.nombre; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.nombre IS 'nombre del sistema';


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.descripcion; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.descripcion IS 'descripcion del sistema';


--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.cant_preg_desafio; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.cant_preg_desafio IS 'cantidad de preguntas de desafio';


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.dias_cambio_clave; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.dias_cambio_clave IS 'dias para el cambio de clave automatico';


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN sistema.activo; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.sistema.activo IS 'indica si el sistema esta o no activo';


--
-- TOC entry 184 (class 1259 OID 17484)
-- Name: sistema_id_sistema_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.sistema_id_sistema_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.sistema_id_sistema_seq OWNER TO postgres;

--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 184
-- Name: sistema_id_sistema_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.sistema_id_sistema_seq OWNED BY autenticacion.sistema.id_sistema;


--
-- TOC entry 198 (class 1259 OID 28894)
-- Name: trazabilidad; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.trazabilidad (
    id_trazabilidad integer NOT NULL,
    id_sistema integer NOT NULL,
    tipo_trazabilidad character varying(16) NOT NULL,
    nombre_tabla character varying(32) NOT NULL,
    id_usuario integer NOT NULL,
    detalle character varying(256) NOT NULL,
    id_tabla character varying(16) NOT NULL,
    fecha_registro date NOT NULL
);


ALTER TABLE autenticacion.trazabilidad OWNER TO postgres;

--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE trazabilidad; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.trazabilidad IS 'Tabla que guarda los movimientos que se realizan en las tablas de usuarios, sistemas, roles y permisos';


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.id_trazabilidad; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.id_trazabilidad IS 'Representa un valor consecutivo y unico que identifica el registro';


--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.id_sistema IS 'id del sistema';


--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.tipo_trazabilidad; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.tipo_trazabilidad IS 'Indica la accion realizada: salvar, actualizar, borrar, Imprimir, descargar, accesar';


--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.nombre_tabla; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.nombre_tabla IS 'Nombre de la tabla donde se realizó el movimiento';


--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.id_usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.id_usuario IS 'id del usuario';


--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.detalle; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.detalle IS 'descripcion detallada de la accion que se realizo';


--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.id_tabla; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.id_tabla IS 'id de la tabla a auditar';


--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN trazabilidad.fecha_registro; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.trazabilidad.fecha_registro IS 'fecha en que se registra el movimiento';


--
-- TOC entry 197 (class 1259 OID 28892)
-- Name: trazabilidad_id_trazabilidad_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.trazabilidad_id_trazabilidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.trazabilidad_id_trazabilidad_seq OWNER TO postgres;

--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 197
-- Name: trazabilidad_id_trazabilidad_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.trazabilidad_id_trazabilidad_seq OWNED BY autenticacion.trazabilidad.id_trazabilidad;


--
-- TOC entry 191 (class 1259 OID 17514)
-- Name: usuario; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.usuario (
    id_usuario integer NOT NULL,
    id_sistema integer NOT NULL,
    login_usuario character varying(32) NOT NULL,
    password character varying(500) NOT NULL,
    id_rol integer NOT NULL,
    nombre character varying(64) NOT NULL,
    e_mail character varying(32) NOT NULL,
    activo boolean NOT NULL,
    fecha_cambio_clave timestamp without time zone DEFAULT '2000-01-01 18:00:00'::timestamp without time zone NOT NULL,
    "debeCambiarClave" boolean DEFAULT true NOT NULL,
    tipo_usuario integer DEFAULT 2 NOT NULL
);


ALTER TABLE autenticacion.usuario OWNER TO postgres;

--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 191
-- Name: TABLE usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.usuario IS 'Tabla que guarda los usuarios de los diferentes sistemas';


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.id_usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.id_usuario IS 'id del usuario';


--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.id_sistema; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.id_sistema IS 'id del sistema';


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.login_usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.login_usuario IS 'login del usuario';


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.password; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.password IS 'password del usuario';


--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.id_rol; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.id_rol IS 'rol por defecto del usuario';


--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.nombre; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.nombre IS 'nombre del usuario';


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.e_mail; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.e_mail IS 'email del usuario';


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.activo; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.activo IS 'indica si el usuario está o no activo';


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario.fecha_cambio_clave; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario.fecha_cambio_clave IS 'indica la fecha en que se cambió la clave al usuario por ultima vez';


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN usuario."debeCambiarClave"; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario."debeCambiarClave" IS 'Indica si el usuario debe cambiar su clave por defecto';


--
-- TOC entry 190 (class 1259 OID 17512)
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: autenticacion; Owner: postgres
--

CREATE SEQUENCE autenticacion.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE autenticacion.usuario_id_usuario_seq OWNER TO postgres;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 190
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: autenticacion; Owner: postgres
--

ALTER SEQUENCE autenticacion.usuario_id_usuario_seq OWNED BY autenticacion.usuario.id_usuario;


--
-- TOC entry 193 (class 1259 OID 17529)
-- Name: usuario_pregunta_desafio; Type: TABLE; Schema: autenticacion; Owner: postgres
--

CREATE TABLE autenticacion.usuario_pregunta_desafio (
    id_pregunta integer NOT NULL,
    id_usuario integer NOT NULL,
    respuesta character varying(64) NOT NULL
);


ALTER TABLE autenticacion.usuario_pregunta_desafio OWNER TO postgres;

--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 193
-- Name: TABLE usuario_pregunta_desafio; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON TABLE autenticacion.usuario_pregunta_desafio IS 'Tabla de relación entre usuario y pregunta_desafio';


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN usuario_pregunta_desafio.id_pregunta; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario_pregunta_desafio.id_pregunta IS 'id de la pregunta de desafio';


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN usuario_pregunta_desafio.id_usuario; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario_pregunta_desafio.id_usuario IS 'id del usuario';


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN usuario_pregunta_desafio.respuesta; Type: COMMENT; Schema: autenticacion; Owner: postgres
--

COMMENT ON COLUMN autenticacion.usuario_pregunta_desafio.respuesta IS 'Respuesta de la pregunta de desafio';


--
-- TOC entry 2045 (class 2604 OID 28840)
-- Name: arcgis id_arcgis; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.arcgis ALTER COLUMN id_arcgis SET DEFAULT nextval('autenticacion.arcgis_id_arcgis_seq'::regclass);


--
-- TOC entry 2036 (class 2604 OID 17500)
-- Name: permiso id_permiso; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.permiso ALTER COLUMN id_permiso SET DEFAULT nextval('autenticacion.permiso_id_permiso_seq'::regclass);


--
-- TOC entry 2031 (class 2604 OID 17481)
-- Name: pregunta_desafio id_pregunta; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.pregunta_desafio ALTER COLUMN id_pregunta SET DEFAULT nextval('autenticacion.pregunta_desafio_id_pregunta_seq'::regclass);


--
-- TOC entry 2037 (class 2604 OID 17509)
-- Name: rol id_rol; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol ALTER COLUMN id_rol SET DEFAULT nextval('autenticacion.rol_id_rol_seq'::regclass);


--
-- TOC entry 2032 (class 2604 OID 17489)
-- Name: sistema id_sistema; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.sistema ALTER COLUMN id_sistema SET DEFAULT nextval('autenticacion.sistema_id_sistema_seq'::regclass);


--
-- TOC entry 2046 (class 2604 OID 28897)
-- Name: trazabilidad id_trazabilidad; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.trazabilidad ALTER COLUMN id_trazabilidad SET DEFAULT nextval('autenticacion.trazabilidad_id_trazabilidad_seq'::regclass);


--
-- TOC entry 2040 (class 2604 OID 17517)
-- Name: usuario id_usuario; Type: DEFAULT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario ALTER COLUMN id_usuario SET DEFAULT nextval('autenticacion.usuario_id_usuario_seq'::regclass);


--
-- TOC entry 2206 (class 0 OID 17524)
-- Dependencies: 192
-- Data for Name: acceso; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--



--
-- TOC entry 2210 (class 0 OID 28837)
-- Dependencies: 196
-- Data for Name: arcgis; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--



--
-- TOC entry 2201 (class 0 OID 17497)
-- Dependencies: 187
-- Data for Name: permiso; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.permiso VALUES (1, 1, 'Permiso 1 Sistema  Promarca ', 'per-1-promarca');
INSERT INTO autenticacion.permiso VALUES (2, 1, 'Permiso 2 Sistema  Promarca ', 'per-2-promarca');
INSERT INTO autenticacion.permiso VALUES (3, 1, 'Permiso 3 Sistema  Promarca ', 'per-3-promarca');
INSERT INTO autenticacion.permiso VALUES (4, 1, 'Permiso 4 Sistema  Promarca ', 'per-4-promarca');
INSERT INTO autenticacion.permiso VALUES (5, 1, 'Permiso 5 Sistema  Promarca ', 'per-5-promarca');
INSERT INTO autenticacion.permiso VALUES (6, 2, 'Permiso 1 Sistema  servisdesk', 'per-1-servisdesk');
INSERT INTO autenticacion.permiso VALUES (7, 2, 'Permiso 2 Sistema  servisdesk', 'per-2-servisdesk');
INSERT INTO autenticacion.permiso VALUES (8, 2, 'Permiso 3 Sistema  servisdesk', 'per-3-servisdesk');
INSERT INTO autenticacion.permiso VALUES (9, 2, 'Permiso 4 Sistema  servisdesk', 'per-4-servisdesk');
INSERT INTO autenticacion.permiso VALUES (10, 2, 'Permiso 5 Sistema  servisdesk', 'per-5-servisdesk');
INSERT INTO autenticacion.permiso VALUES (11, 3, 'Permiso 1 Sistema  Directv ', 'per-1-directv');
INSERT INTO autenticacion.permiso VALUES (12, 3, 'Permiso 2 Sistema  Directv ', 'per-2-directv');
INSERT INTO autenticacion.permiso VALUES (13, 3, 'Permiso 3 Sistema  Directv ', 'per-3-directv');
INSERT INTO autenticacion.permiso VALUES (14, 3, 'Permiso 4 Sistema  Directv ', 'per-4-directv');
INSERT INTO autenticacion.permiso VALUES (15, 3, 'Permiso 5 Sistema  Directv ', 'per-5-directv');


--
-- TOC entry 2197 (class 0 OID 17478)
-- Dependencies: 183
-- Data for Name: pregunta_desafio; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.pregunta_desafio VALUES (1, 'Cuál es el nombre de tu primera mascota?');
INSERT INTO autenticacion.pregunta_desafio VALUES (2, 'Cuál es tu comida favorita?');
INSERT INTO autenticacion.pregunta_desafio VALUES (3, 'Cuál es el apellido de soltera de tu madre?');
INSERT INTO autenticacion.pregunta_desafio VALUES (4, 'Cual sería tu trabajo ideal?');
INSERT INTO autenticacion.pregunta_desafio VALUES (5, 'Descripcion PreguntaDesafio-5');
INSERT INTO autenticacion.pregunta_desafio VALUES (6, 'Cual era el apodo de tu amigo(a) de la secundaria');
INSERT INTO autenticacion.pregunta_desafio VALUES (7, 'Cual es el apodo de tu ahijado(a)?');


--
-- TOC entry 2203 (class 0 OID 17506)
-- Dependencies: 189
-- Data for Name: rol; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.rol VALUES (2, 1, 'Descripcion del rol 2', 'Titular de Unidad(Promarca)');
INSERT INTO autenticacion.rol VALUES (3, 1, 'Descripcion del rol 3', 'Titular de area(Promarca)');
INSERT INTO autenticacion.rol VALUES (4, 1, 'Descripcion del rol 4', 'Suplente de unidad(Promarca)');
INSERT INTO autenticacion.rol VALUES (5, 1, 'Descripcion del rol 5', 'Suplente de area(Promarca)');
INSERT INTO autenticacion.rol VALUES (6, 2, 'Descripcion del rol 6', 'Administardor de Sistemas(Servisdesk)');
INSERT INTO autenticacion.rol VALUES (7, 2, 'Descripcion del rol 7', 'Titular de Unidad(Servisdesk)');
INSERT INTO autenticacion.rol VALUES (8, 2, 'Descripcion del rol 8', 'Titular de area(Servisdesk)');
INSERT INTO autenticacion.rol VALUES (9, 2, 'Descripcion del rol 9', 'Suplente de unidad(Servisdesk)');
INSERT INTO autenticacion.rol VALUES (10, 2, 'Descripcion del rol 10', 'Suplente de area(Servisdesk)');
INSERT INTO autenticacion.rol VALUES (11, 1, 'Descripcion del rol 11', 'rol 11 de promarca');
INSERT INTO autenticacion.rol VALUES (13, 1, 'Rol 2 de promarca', 'Rol 2 de promarca');
INSERT INTO autenticacion.rol VALUES (15, 1, 'Rol 3 de promarca', 'Rol 3 de promarca');
INSERT INTO autenticacion.rol VALUES (16, 1, 'Rol 3 de promarca', 'Rol 3 de promarca');
INSERT INTO autenticacion.rol VALUES (17, 1, 'Rol 3 de promarca', 'Rol 3 de promarca');
INSERT INTO autenticacion.rol VALUES (18, 1, 'Rol 3 de promarca', 'Rol 3 de promarca');
INSERT INTO autenticacion.rol VALUES (20, 1, 'Descripcion del rol 2', 'Descripcion del rol 2');
INSERT INTO autenticacion.rol VALUES (1, 1, 'Administardor de Sistemas(Promarca)', 'Administardor de Sistemas');
INSERT INTO autenticacion.rol VALUES (21, 2, 'Administardor de Sistemas(Promarca)', 'Administardor de Sistemas');
INSERT INTO autenticacion.rol VALUES (22, 37, 'xxxxxxxxxxxxxxxxxxxxxx', 'zzzzzzzzzzzzzzzzzzzzzz');


--
-- TOC entry 2208 (class 0 OID 17534)
-- Dependencies: 194
-- Data for Name: rol_permiso; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.rol_permiso VALUES (1, 1);
INSERT INTO autenticacion.rol_permiso VALUES (1, 2);
INSERT INTO autenticacion.rol_permiso VALUES (1, 3);
INSERT INTO autenticacion.rol_permiso VALUES (2, 1);
INSERT INTO autenticacion.rol_permiso VALUES (2, 3);
INSERT INTO autenticacion.rol_permiso VALUES (2, 5);


--
-- TOC entry 2199 (class 0 OID 17486)
-- Dependencies: 185
-- Data for Name: sistema; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.sistema VALUES (1, 'Promarca', 'descripcion promarca', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (3, 'DirecTV', 'Direc TV entretenimiento', 1, 30, true);
INSERT INTO autenticacion.sistema VALUES (7, 'Promarca2', 'Sistema de promarca dos', 1, 90, true);
INSERT INTO autenticacion.sistema VALUES (4, 'De Candido', 'Descripcion Sistema-4', 2, 90, true);
INSERT INTO autenticacion.sistema VALUES (2, 'Supermart', 'descripcion Servisdesk', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (59, 'nnnnnnnnnnnnnnnn', 'nnnnnnnnnnnnnnnnn', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (5, 'sysmodelers', 'El sistema de los vergatarios', 2, 120, true);
INSERT INTO autenticacion.sistema VALUES (33, 'aaaaaaaa', 'aaaaaaaaa', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (34, 'aaaaaaaa', 'aaaaaa', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (35, 'aaaa', 'bbbb', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (36, 'aaaa', 'bb', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (38, 'eee', 'eeee', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (53, 'bbb333333333333333333333333', '3333333333333', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (55, 'nuevo sistema', 'nuevo sistema', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (60, 'SSistema sistema', 'SSistema sistema', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (40, 'Sistema de prueba para bitacoras', 'descripcion de pruebas para bitacoras', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (41, 'Sistema de prueba para bitacorassss', 'descripcion de pruebas para bitacorasss', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (47, 'Sistema de dddddddpara bitacorassss', 'descripcion de pruebas para bitacorasss', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (6, 'sysmodelers2', 'Sistema sismodelers 2', 1, 90, true);
INSERT INTO autenticacion.sistema VALUES (37, 'sistema A', 'Descripcion A', 1, 90, true);
INSERT INTO autenticacion.sistema VALUES (50, 'xxxxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxx', 1, 30, true);
INSERT INTO autenticacion.sistema VALUES (52, 'ccccccccccccccc', 'ccccccccccccccccc', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (54, 'rrrrrr', 'rrrrrr', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (56, 'qqqqqqqqq', 'qqqqqqqqqqq', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (51, 'zzzzzzzzzzz', 'zzzzzzzzzz', 0, 0, true);
INSERT INTO autenticacion.sistema VALUES (58, 'ggggggggggggggggggggggggg', 'ggggggggggggggggggggg', 0, 0, true);


--
-- TOC entry 2212 (class 0 OID 28894)
-- Dependencies: 198
-- Data for Name: trazabilidad; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.trazabilidad VALUES (1, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre Sistema de dddddddpara bitacorassss descripcion ddddescripcion de pruebas para bitacorasss', '49', '2020-03-29');
INSERT INTO autenticacion.trazabilidad VALUES (2, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre sistema Aeeeeeeeeeeeeee descripcion Descripcion eeeeeeeeee', '37', '2020-04-02');
INSERT INTO autenticacion.trazabilidad VALUES (3, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre sistema A descripcion Descripcion A', '37', '2020-04-10');
INSERT INTO autenticacion.trazabilidad VALUES (4, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre sistema A descripcion Descripcion A', '37', '2020-04-10');
INSERT INTO autenticacion.trazabilidad VALUES (5, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre Sistema de dddddddpara bitacorassss descripcion descripcion de pruebas para bitacorasss', '47', '2020-04-10');
INSERT INTO autenticacion.trazabilidad VALUES (6, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbbbbbbbbbbbb descripcion bbbbbbbbbb', '53', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (7, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbbbbbbbbbbbb descripcion bbbbbbbbbb', '53', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (8, 1, 'GUARDAR', 'Sistema', 1, ' nombre ggggggggggggggggggggggggg descripcion ggggggggggggggggggggg', '58', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (9, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbbbbbbbbbbbb descripcion bbbbbbbbbb', '53', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (11, 1, 'GUARDAR', 'Sistema', 1, ' nombre nnnnnnnnnnnnnnnn descripcion nnnnnnnnnnnnnnnnn', '59', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (12, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre ggggggggg descripcion gggggggg', '57', '2020-04-13');
INSERT INTO autenticacion.trazabilidad VALUES (13, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbbbbbbbbbbbb descripcion bbbbbbbbbb', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (14, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbbbbbbbbbbbb descripcion bbbbbbbbbb', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (15, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbb333333333333333333333333 descripcion bbbbbbbbbb', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (16, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbb333333333333333333333333 descripcion bbbbbbbbbb', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (17, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbb333333333333333333333333 descripcion bbbbbbbbbb', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (18, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre bbb333333333333333333333333 descripcion 3333333333333', '53', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (19, 1, 'ACTUALIZAR', 'Sistema', 1, ' nombre nuevo sistema descripcion nuevo sistema', '55', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (20, 1, 'GUARDAR', 'Sistema', 1, ' nombre SSistema sistema descripcion SSistema sistema', '60', '2020-04-14');
INSERT INTO autenticacion.trazabilidad VALUES (21, 1, 'BORRAR', 'Sistema', 1, ' nombre ggggggggg descripcion gggggggg', '57', '2020-04-14');


--
-- TOC entry 2205 (class 0 OID 17514)
-- Dependencies: 191
-- Data for Name: usuario; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.usuario VALUES (2, 1, 'jose', '123', 1, 'jose', 'josemail', true, '2000-01-12 18:00:00', true, 2);
INSERT INTO autenticacion.usuario VALUES (3, 2, 'Herrera', '123', 2, 'herrera', 'eeee', true, '2000-01-12 18:00:00', true, 2);
INSERT INTO autenticacion.usuario VALUES (1, 1, 'adaulfo', '123', 1, 'Adaulfo Nombre', 'adaulfoemail', true, '2000-01-12 18:00:00', true, 0);


--
-- TOC entry 2207 (class 0 OID 17529)
-- Dependencies: 193
-- Data for Name: usuario_pregunta_desafio; Type: TABLE DATA; Schema: autenticacion; Owner: postgres
--

INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (1, 1, '123');
INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (2, 1, '123');
INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (3, 1, '123');
INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (2, 2, '123');
INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (4, 2, '123');
INSERT INTO autenticacion.usuario_pregunta_desafio VALUES (3, 3, '123');


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 195
-- Name: arcgis_id_arcgis_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.arcgis_id_arcgis_seq', 1, false);


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 186
-- Name: permiso_id_permiso_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.permiso_id_permiso_seq', 1, false);


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 182
-- Name: pregunta_desafio_id_pregunta_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.pregunta_desafio_id_pregunta_seq', 7, true);


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 188
-- Name: rol_id_rol_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.rol_id_rol_seq', 38, true);


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 184
-- Name: sistema_id_sistema_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.sistema_id_sistema_seq', 60, true);


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 197
-- Name: trazabilidad_id_trazabilidad_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.trazabilidad_id_trazabilidad_seq', 21, true);


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 190
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: autenticacion; Owner: postgres
--

SELECT pg_catalog.setval('autenticacion.usuario_id_usuario_seq', 2, true);


--
-- TOC entry 2060 (class 2606 OID 17528)
-- Name: acceso acceso_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.acceso
    ADD CONSTRAINT acceso_pk PRIMARY KEY (id_usuario, id_rol);


--
-- TOC entry 2066 (class 2606 OID 28842)
-- Name: arcgis arcgis_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.arcgis
    ADD CONSTRAINT arcgis_pk PRIMARY KEY (id_arcgis);


--
-- TOC entry 2053 (class 2606 OID 17502)
-- Name: permiso permiso_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.permiso
    ADD CONSTRAINT permiso_pk PRIMARY KEY (id_permiso);


--
-- TOC entry 2048 (class 2606 OID 17483)
-- Name: pregunta_desafio pregunta_desafio_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.pregunta_desafio
    ADD CONSTRAINT pregunta_desafio_pk PRIMARY KEY (id_pregunta);


--
-- TOC entry 2064 (class 2606 OID 17538)
-- Name: rol_permiso rol_permiso_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol_permiso
    ADD CONSTRAINT rol_permiso_pk PRIMARY KEY (id_rol, id_permiso);


--
-- TOC entry 2055 (class 2606 OID 17511)
-- Name: rol rol_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id_rol);


--
-- TOC entry 2050 (class 2606 OID 17494)
-- Name: sistema sistema_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.sistema
    ADD CONSTRAINT sistema_pk PRIMARY KEY (id_sistema);


--
-- TOC entry 2068 (class 2606 OID 28899)
-- Name: trazabilidad trazabilidad_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.trazabilidad
    ADD CONSTRAINT trazabilidad_pk PRIMARY KEY (id_trazabilidad);


--
-- TOC entry 2057 (class 2606 OID 17522)
-- Name: usuario usuario_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);


--
-- TOC entry 2062 (class 2606 OID 17533)
-- Name: usuario_pregunta_desafio usuario_pregunta_desafio_pk; Type: CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario_pregunta_desafio
    ADD CONSTRAINT usuario_pregunta_desafio_pk PRIMARY KEY (id_pregunta, id_usuario);


--
-- TOC entry 2051 (class 1259 OID 17737)
-- Name: permiso_idx; Type: INDEX; Schema: autenticacion; Owner: postgres
--

CREATE UNIQUE INDEX permiso_idx ON autenticacion.permiso USING btree (nombre_corto, id_sistema);


--
-- TOC entry 2058 (class 1259 OID 17523)
-- Name: usuario_sistema_idx; Type: INDEX; Schema: autenticacion; Owner: postgres
--

CREATE UNIQUE INDEX usuario_sistema_idx ON autenticacion.usuario USING btree (login_usuario);


--
-- TOC entry 2077 (class 2606 OID 17559)
-- Name: rol_permiso permisos_rol_permiso_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol_permiso
    ADD CONSTRAINT permisos_rol_permiso_fk FOREIGN KEY (id_permiso) REFERENCES autenticacion.permiso(id_permiso);


--
-- TOC entry 2075 (class 2606 OID 17539)
-- Name: usuario_pregunta_desafio pregunta_desafio_usuario_pregunta_desafio_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario_pregunta_desafio
    ADD CONSTRAINT pregunta_desafio_usuario_pregunta_desafio_fk FOREIGN KEY (id_pregunta) REFERENCES autenticacion.pregunta_desafio(id_pregunta);


--
-- TOC entry 2073 (class 2606 OID 17574)
-- Name: acceso rol_acceso_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.acceso
    ADD CONSTRAINT rol_acceso_fk FOREIGN KEY (id_rol) REFERENCES autenticacion.rol(id_rol);


--
-- TOC entry 2072 (class 2606 OID 17569)
-- Name: usuario rol_usuario_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario
    ADD CONSTRAINT rol_usuario_fk FOREIGN KEY (id_rol) REFERENCES autenticacion.rol(id_rol);


--
-- TOC entry 2078 (class 2606 OID 17564)
-- Name: rol_permiso roles_rol_permiso_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol_permiso
    ADD CONSTRAINT roles_rol_permiso_fk FOREIGN KEY (id_rol) REFERENCES autenticacion.rol(id_rol);


--
-- TOC entry 2069 (class 2606 OID 17544)
-- Name: permiso sistema_permisos_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.permiso
    ADD CONSTRAINT sistema_permisos_fk FOREIGN KEY (id_sistema) REFERENCES autenticacion.sistema(id_sistema);


--
-- TOC entry 2070 (class 2606 OID 17549)
-- Name: rol sistema_rol_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.rol
    ADD CONSTRAINT sistema_rol_fk FOREIGN KEY (id_sistema) REFERENCES autenticacion.sistema(id_sistema);


--
-- TOC entry 2079 (class 2606 OID 28843)
-- Name: arcgis sistema_sistema_arcgis_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.arcgis
    ADD CONSTRAINT sistema_sistema_arcgis_fk FOREIGN KEY (id_sistema) REFERENCES autenticacion.sistema(id_sistema);


--
-- TOC entry 2080 (class 2606 OID 28900)
-- Name: trazabilidad sistema_trazabilidad_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.trazabilidad
    ADD CONSTRAINT sistema_trazabilidad_fk FOREIGN KEY (id_sistema) REFERENCES autenticacion.sistema(id_sistema);


--
-- TOC entry 2071 (class 2606 OID 17554)
-- Name: usuario sistema_usuario_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario
    ADD CONSTRAINT sistema_usuario_fk FOREIGN KEY (id_sistema) REFERENCES autenticacion.sistema(id_sistema);


--
-- TOC entry 2074 (class 2606 OID 17584)
-- Name: acceso usuario_acceso_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.acceso
    ADD CONSTRAINT usuario_acceso_fk FOREIGN KEY (id_usuario) REFERENCES autenticacion.usuario(id_usuario);


--
-- TOC entry 2076 (class 2606 OID 17579)
-- Name: usuario_pregunta_desafio usuario_sistema_usuario_pregunta_desafio_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.usuario_pregunta_desafio
    ADD CONSTRAINT usuario_sistema_usuario_pregunta_desafio_fk FOREIGN KEY (id_usuario) REFERENCES autenticacion.usuario(id_usuario);


--
-- TOC entry 2081 (class 2606 OID 28905)
-- Name: trazabilidad usuario_trazabilidad_fk; Type: FK CONSTRAINT; Schema: autenticacion; Owner: postgres
--

ALTER TABLE ONLY autenticacion.trazabilidad
    ADD CONSTRAINT usuario_trazabilidad_fk FOREIGN KEY (id_usuario) REFERENCES autenticacion.usuario(id_usuario);


--
-- TOC entry 2218 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-04-14 09:53:56

--
-- PostgreSQL database dump complete
--

